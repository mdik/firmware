/***************************************************
  Arduino library written for the Maxim MAX86150 ECG and PPG integrated sensor

	Written by Ashwin Whitchurch, ProtoCentral Electronics (www.protocentral.com)

	https://github.com/protocentral/protocentral_max86150

  Based on code written by Peter Jansen and Nathan Seidle (SparkFun) for the MAX30105 sensor
  BSD license, all text above must be included in any redistribution.
 *****************************************************/

#pragma once

#define MAX86150_ADDRESS          0x5E //7-bit I2C Address

#define I2C_BUFFER_LENGTH 64

#include <stdint.h>
#include <stdbool.h>


static const uint8_t MAX86150_SAMPLEAVG_1 = 	0b000;
static const uint8_t MAX86150_SAMPLEAVG_2 = 	0b001;
static const uint8_t MAX86150_SAMPLEAVG_4 = 	0b010;
static const uint8_t MAX86150_SAMPLEAVG_8 = 	0b011;
static const uint8_t MAX86150_SAMPLEAVG_16 = 	0b100;
static const uint8_t MAX86150_SAMPLEAVG_32 = 	0b101;

static const uint8_t MAX86150_ADCRANGE_4096 = 	0b00000000;
static const uint8_t MAX86150_ADCRANGE_8192 = 	0b01000000;
static const uint8_t MAX86150_ADCRANGE_16384 = 	0b10000000;
static const uint8_t MAX86150_ADCRANGE_32768 = 	0b11000000;

static const uint8_t MAX86150_PPG_SAMPLERATE_10 =	0b00000000;
static const uint8_t MAX86150_PPG_SAMPLERATE_20 =	0b00000100;
static const uint8_t MAX86150_PPG_SAMPLERATE_50 =	0b00001000;
static const uint8_t MAX86150_PPG_SAMPLERATE_84 =	0b00001100;
static const uint8_t MAX86150_PPG_SAMPLERATE_100 =	0b00010000;
static const uint8_t MAX86150_PPG_SAMPLERATE_200 =	0b00010100;
static const uint8_t MAX86150_PPG_SAMPLERATE_400 =	0b00011000;
static const uint8_t MAX86150_PPG_SAMPLERATE_800 =	0b00011100;
static const uint8_t MAX86150_PPG_SAMPLERATE_1000 =	0b00100000;
static const uint8_t MAX86150_PPG_SAMPLERATE_1600 =	0b00100100;
static const uint8_t MAX86150_PPG_SAMPLERATE_3200 =	0b00101000;

static const uint8_t MAX86150_PPG_PULSEWIDTH_50 =	0b00;
static const uint8_t MAX86150_PPG_PULSEWIDTH_100 =	0b01;
static const uint8_t MAX86150_PPG_PULSEWIDTH_200 =	0b10;
static const uint8_t MAX86150_PPG_PULSEWIDTH_400 =	0b11;

static const uint8_t MAX86150_SLOT_NONE =	0b0000;
static const uint8_t MAX86150_SLOT_RED_LED =	0b0010;
static const uint8_t MAX86150_SLOT_IR_LED =	0b0001;
static const uint8_t MAX86150_SLOT_RED_PILOT =	0b0110;
static const uint8_t MAX86150_SLOT_IR_PILOT =	0b0101;
static const uint8_t MAX86150_SLOT_ECG =	0b1001;

static const uint8_t MAX86150_LED1_RANGE_50 =	0b00;
static const uint8_t MAX86150_LED1_RANGE_100 =	0b01;
static const uint8_t MAX86150_LED2_RANGE_50 =	0b0000;
static const uint8_t MAX86150_LED2_RANGE_100 =	0b0100;

static const uint8_t MAX86150_ECG_SAMPLERATE_200 =	0b011;
static const uint8_t MAX86150_ECG_SAMPLERATE_400 =	0b010;
static const uint8_t MAX86150_ECG_SAMPLERATE_800 =	0b001;
static const uint8_t MAX86150_ECG_SAMPLERATE_1600 =	0b000;
static const uint8_t MAX86150_ECG_SAMPLERATE_3200 =	0b100;

static const uint8_t MAX86150_ECG_PGA_GAIN_1 =		0b0000;
static const uint8_t MAX86150_ECG_PGA_GAIN_2 =		0b0100;
static const uint8_t MAX86150_ECG_PGA_GAIN_4 =		0b1000;
static const uint8_t MAX86150_ECG_PGA_GAIN_8 =		0b1100;

static const uint8_t MAX86150_ECG_IA_GAIN_5 =		0b00;
static const uint8_t MAX86150_ECG_IA_GAIN_9_5 =		0b01;
static const uint8_t MAX86150_ECG_IA_GAIN_20 =		0b10;
static const uint8_t MAX86150_ECG_IA_GAIN_50 =		0b11;



bool max86150_begin(void);

uint8_t max86150_get_int1(void);
uint8_t max86150_get_int2(void);

void max86150_set_int_full(bool enabled);
void max86150_set_int_datardy(bool enabled);
void max86150_set_int_ambient_light_overflow(bool enabled);
void max86150_set_int_proximity(bool enabled);

void max86150_soft_reset(void);
void max86150_shut_down(void);
void max86150_wake_up(void);
void max86150_set_ppg_adc_range(uint8_t adcRange);
void max86150_set_ppg_sample_rate(uint8_t sampleRate);
void max86150_set_ppg_pulse_width(uint8_t pulseWidth);
void max86150_set_led_red_amplitude(uint8_t amplitude);
void max86150_set_led_ir_amplitude(uint8_t amplitude);
void max86150_set_led_proximity_amplitude(uint8_t amplitude);
void max86150_set_proximity_threshold(uint8_t threshMSB);
void max86150_fifo_enable_slot(uint8_t slotNumber, uint8_t device);
void max86150_disableSlots(void);
void max86150_set_ppg_averaging(uint8_t numberOfSamples);
void max86150_clear_fifo(void);
void max86150_set_fifo_rollover(bool enabled);
void max86150_set_fifo_almost_full_clear(bool enabled);
void max86150_set_fifo_almost_full_repeat(bool enabled);
void max86150_set_fifo_almost_full(uint8_t numberOfSamples);
uint8_t max86150_get_fifo_write_pointer(void);
uint8_t max86150_get_fifo_read_pointer(void);
uint8_t max86150_read_part_id();
void max86150_set_ecg_sample_rate(uint8_t sampleRate);
void max86150_set_ecg_pga_gain(uint8_t gain);
void max86150_set_ecg_instrumentation_amplifier_gain(uint8_t gain);

void max86150_setup(const uint8_t ppg_sample_rate);

uint8_t max86150_available(void);
uint32_t max86150_get_red(void);
uint32_t max86150_get_ir(void);
int32_t max86150_get_ecg(void);
uint32_t max86150_get_fifo_red(void);
uint32_t max86150_get_fifo_ir(void);
int32_t max86150_get_fifo_ecg(void);
void max86150_next_sample(void);
uint8_t max86150_get_sample(uint32_t *red, uint32_t *ir, int32_t *ecg);
uint16_t max86150_check(void);
bool max86150_safe_check(uint8_t max_tries);

void max86150_bit_mask(uint8_t reg, uint8_t mask, uint8_t thing);
uint8_t max86150_read_register(uint8_t address, uint8_t reg);
void max86150_write_register(uint8_t address, uint8_t reg, uint8_t value);
